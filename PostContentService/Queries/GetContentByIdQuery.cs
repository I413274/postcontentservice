﻿using MediatR;
using PostContentService.models;

namespace PostContentService.Queries
{
    public class GetContentByIdQuery : IRequest<ContentModel>
    {
        public int Id { get; set; }
    }
}
