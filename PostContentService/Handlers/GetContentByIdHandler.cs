﻿using MediatR;
using PostContentService.models;
using PostContentService.Queries;
using PostContentService.repository;
using Serilog;

namespace PostContentService.Handlers
{
    public class GetContentByIdHandler : IRequestHandler<GetContentByIdQuery, ContentModel>
    {
        private readonly IContentRepository _contentRepository;

        public GetContentByIdHandler(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public async Task<ContentModel> Handle(GetContentByIdQuery query, CancellationToken cancellationToken)
        {
            try
            {
                Log.Information("Get content by ID from the database");
                return await _contentRepository.GetContentByIdAsync(query.Id);
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while retrieving content by ID from the database");
                throw;
                // Optionally, perform error handling or rethrow the exception
                // throw;
            }
        }

    }
}
