﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PostContentService.Commands;
using PostContentService.Events;
using PostContentService.models;
using PostContentService.Queries;
using Serilog;
using System.Data;

namespace PostContentService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
    public class PostContentController
    {
        private readonly IMediator mediator;

        public PostContentController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<List<ContentModel>> Get()
        {
            var content = await mediator.Send(new GetContentListQuery());
            return content;
        }

        [HttpPost]
        public async Task<ContentModel> AddContentAsync(ContentModel content)
        {
            Log.Information("Send content to database");
            var contentmodel = await mediator.Send(new CreateContentCommand(
                 content.Id,
                    content.UserID,
                    content.Name,
                    content.Title,
                    content.Description));
            Log.Information("Send content to fetch service");
            if (contentmodel != null)
            {
                await mediator.Publish(new ContentCreatedEvent(
                    content.Id,
                    content.UserID,
                    content.Name,
                    content.Title,
                    content.Description));
            }
            return contentmodel;
        }

        [HttpPut]
        public async Task<int> UpdateContentAsync(ContentModel content)
        {
            var isContentUpdated = await mediator.Send(new UpdateContentCommand(
                    content.Id,
                    content.UserID,
                    content.Name,
                    content.Title,
                    content.Description));

            return isContentUpdated;
        }

        [HttpDelete]
        public async Task<int> DeleteContentAsync(int Id)
        {
            return await mediator.Send(new DeleteContentCommand() { Id = Id });
        }
    }
}
