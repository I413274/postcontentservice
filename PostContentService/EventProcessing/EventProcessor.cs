﻿using Microsoft.Extensions.Caching.Distributed;
using PostContentService.repository;
using System.Text.Json;

namespace PostContentService.EventProcessing
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IContentRepository _contentRepository;
        public EventProcessor()
        {
            
        }
        public void ProcessEvent(string message)
        {
            var contentPublishedDto = JsonSerializer.Deserialize<string>(message);

            

        }

        private bool IsDeleteAccountEvent(string eventMessage)
        {
            // Implement your logic to identify the delete account event based on the event message
            // Return true if it's a delete account event, false otherwise
            // ...
            throw new NotImplementedException();
        }

        private void PerformDeleteAccountAction(string id)
        {
            _contentRepository.DeleteContentByUserIdAsync(id);
            // Implement your specific action for delete account event
            // ...
            throw new NotImplementedException();
        }
    }
}