﻿using PostContentService.data;
using PostContentService.models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Numerics;

namespace PostContentService.repository
{
    public class ContentRepository : IContentRepository
    {
        private readonly DbContextClass _dbContext;

        public ContentRepository(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ContentModel> AddContentAsync(ContentModel model)
        {
            var result = _dbContext.ContentModels.Add(model);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<int> DeleteContentAsync(int Id)
        {
            var filteredData = _dbContext.ContentModels.Where(x => x.Id == Id).FirstOrDefault();
            _dbContext.ContentModels.Remove(filteredData);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<ContentModel> GetContentByIdAsync(int Id)
        {
            return await _dbContext.ContentModels.Where(x => x.Id == Id).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteContentByUserIdAsync(string userid)
        {
            var filteredData = _dbContext.ContentModels.Where(x => x.UserID == userid).ToList();
            foreach (var obj in filteredData)
            {
                obj.UserID = string.Empty;
                obj.Name = "DELETED";
            }
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<ContentModel>> GetContentListAsync()
        {
            return await _dbContext.ContentModels.ToListAsync();
        }

        public async Task<int> UpdateContentAsync(ContentModel studentDetails)
        {
            _dbContext.ContentModels.Update(studentDetails);
            return await _dbContext.SaveChangesAsync();
        }
    }
}
