﻿using MediatR;
using PostContentService.models;

namespace PostContentService.Queries
{
    public class GetContentListQuery : IRequest<List<ContentModel>>
    {
    }
}
