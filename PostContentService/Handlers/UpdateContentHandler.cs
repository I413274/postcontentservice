﻿using MediatR;
using PostContentService.Commands;
using PostContentService.models;
using PostContentService.repository;
using Serilog;

namespace PostContentService.Handlers
{
    public class UpdateContentHandler : IRequestHandler<UpdateContentCommand, int>
    {
        private readonly IContentRepository _contentRepository;

        public UpdateContentHandler(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }
        public async Task<int> Handle(UpdateContentCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var content = await _contentRepository.GetContentByIdAsync(command.Id);
                if (content == null)
                    return default;

                content.Title = command.Title;
                content.Description = command.Description;

                Log.Information("Update content in the database");

                return await _contentRepository.UpdateContentAsync(content);
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while updating content in the database");
                throw;
            }
        }

    }
}
