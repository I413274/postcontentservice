﻿using MediatR;
using PostContentService.models;
using PostContentService.Queries;
using PostContentService.repository;
using Serilog;

namespace PostContentService.Handlers
{
    public class GetContentListHandler : IRequestHandler<GetContentListQuery , List<ContentModel>>
    {
        private readonly IContentRepository _contentRepository;

        public GetContentListHandler(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public async Task<List<ContentModel>> Handle(GetContentListQuery query, CancellationToken cancellationToken)
        {
            try
            {
                Log.Information("Get list of contents from the database");
                return await _contentRepository.GetContentListAsync();
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while retrieving the list of contents from the database");
                throw;
            }
        }

    }
}
