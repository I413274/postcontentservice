﻿using MediatR;

namespace PostContentService.Commands
{
    public class DeleteContentCommand : IRequest<int>
    {
        public int Id { get; set; }
    }
}
