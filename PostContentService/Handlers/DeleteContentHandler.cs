﻿using MediatR;
using PostContentService.Commands;
using PostContentService.repository;
using Serilog;

namespace PostContentService.Handlers
{
    public class DeleteContentHandler : IRequestHandler<DeleteContentCommand, int>
    {
        private readonly IContentRepository _contentRepository;

        public DeleteContentHandler(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public async Task<int> Handle(DeleteContentCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var content = await _contentRepository.GetContentByIdAsync(command.Id);
                if (content == null)
                    return default;

                Log.Information("Delete content from database");

                return await _contentRepository.DeleteContentAsync(content.Id);
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while deleting content from the database");
                throw;
                // Optionally, perform error handling or rethrow the exception
                // throw;
            }
        }

    }
}
