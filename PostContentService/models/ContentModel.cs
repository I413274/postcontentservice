﻿namespace PostContentService.models
{
    public class ContentModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }   
}
