﻿using MediatR;
using PostContentService.models;

namespace PostContentService.Queries
{
    public class CreateContentCommand : IRequest<ContentModel>
    {
        public CreateContentCommand(int id, string userID, string name, string title, string description)
        {
            Id = id;
            UserID = userID;
            Name = name;
            Title = title;
            Description = description;
        }

        public int Id { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
