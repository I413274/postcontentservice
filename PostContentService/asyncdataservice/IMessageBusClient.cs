﻿using PostContentService.Events;
using PostContentService.models;

namespace PostContentService.asyncdataservice
{
    public interface IMessageBusClient
    {
        void PublishContentModel(ContentCreatedEvent content);
    }
}
