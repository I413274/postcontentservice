﻿using PostContentService.Events;
using PostContentService.models;
using RabbitMQ.Client;
using Serilog;
using System.Text;
using System.Text.Json;
using System.Threading.Channels;

namespace PostContentService.asyncdataservice
{
    public class MessageBusClient : IMessageBusClient
    {
        private readonly IConfiguration _configuration;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public MessageBusClient(IConfiguration configuration)
        {
            try
            {
                _configuration = configuration;
                var factory = new ConnectionFactory()
                {
                    HostName = _configuration["RabbitMQHost"],
                    Port = int.Parse(_configuration["RabbitMQPort"])
                };
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: "FetchContent", type: ExchangeType.Topic, true);
                //_channel.QueueDeclare("fetchqueue",
                //durable: true,
                //exclusive: false,
                //autoDelete: false,
                //arguments: null);
                _connection.ConnectionShutdown += RabbitMQ_ConnectionShutDown;
                Log.Information("Connected to the Message Bus");
            }
            catch(Exception ex) 
            {
                Log.Error($"Could not connect to the Message Bus: {ex.Message}");
            }
        }
        public void PublishContentModel(ContentCreatedEvent content) 
        {
            content.Event = "Content_Published";
            var message = JsonSerializer.Serialize(content);

            if (_connection.IsOpen) 
            {
                Log.Information("RabbitMQ Connection Open, sending message");
                SendMessage(message);
            }
            else
            {
                Log.Information("RabbitMQ connection is closed, not sending");
            }
        }

        private void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "FetchContent", 
                routingKey: "fyves",
                basicProperties: null,
                body: body);
            Console.WriteLine($"We have sent {message}");
        }

        public void Dispose()
        {
            Console.WriteLine("MessageBus Disposed");
            if (_channel.IsOpen) 
            {
                _channel.Close();
                _connection.Close();
            }
        }

        private void RabbitMQ_ConnectionShutDown(object sender, ShutdownEventArgs e) 
        {
            Console.WriteLine("RabbitMQ Connection Shutdown");
        }
    }
}
