﻿using PostContentService.models;

namespace PostContentService.repository
{
    public interface IContentRepository
    {
        public Task<List<ContentModel>> GetContentListAsync();
        public Task<ContentModel> GetContentByIdAsync(int Id);
        public Task<ContentModel> AddContentAsync(ContentModel studentDetails);
        public Task<int> UpdateContentAsync(ContentModel studentDetails);
        public Task<int> DeleteContentAsync(int Id);
        public Task<int> DeleteContentByUserIdAsync(string userid);
    }
}
