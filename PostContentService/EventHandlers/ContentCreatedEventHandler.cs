﻿using MassTransit;
using MediatR;
using PostContentService.asyncdataservice;
using Serilog;

namespace PostContentService.Events
{
    public class ContentCreatedEventHandler : INotificationHandler<ContentCreatedEvent>
    {
        private readonly IMessageBusClient _messageBusClient;

        public ContentCreatedEventHandler(IMessageBusClient messageBusClient)
        {
            _messageBusClient = messageBusClient;
        }

        public async Task Handle(ContentCreatedEvent notification, CancellationToken cancellationToken)
        {
            // Publish the event to RabbitMQ
            try
            {
                Log.Information("publish content to message queue");
                _messageBusClient.PublishContentModel(notification);
            }            
            catch (Exception ex)
            {
                Log.Error($"could not send {ex.Message}");
            }
        }
    }
}