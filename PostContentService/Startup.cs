﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using PostContentService.asyncdataservice;
using PostContentService.AsyncDataServices;
using PostContentService.Authentication;
using PostContentService.data;
using PostContentService.EventProcessing;
using PostContentService.repository;
using Serilog;
using Serilog.Sinks.GoogleCloudLogging;

public class Startup
{
    public IConfiguration Configuration { get; }

    private readonly IWebHostEnvironment _env;

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
        Configuration = configuration;
        _env = env;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddCors(c =>
        {
            c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        });

        //services.ConfigureJWT(_env.IsDevelopment(), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAumfnNwzb/yUiD//CsPbjz+4tkkWEwzOnqHSaSnWqUxFL+6iLVAzUm6yvGC09gdaZ6vL6/K1u5SWGk2NRmvH/zn2j58CEKVQCpA3goB+79jH6rREtwCHNRPxyjFrSgukTTqxqGC+yrvNETP7EmJqfpO/SnzWVlGYYgj4rtCqAtbLxLCumiFEkWFf29Lx/5uvKBMHSZ7RnuhVbG73y0s0W347oBVPBF6rh3Bx+pNAHmIheJGsP6OM8SA2n8NLAxq7Iu0UDi0cH0gNs+l+/LH3EubAPl0ydC9hFvP89tZ5RM3Z1dNfegEFcmy3iO78sh/4Lxv0tcS1c3BcSSobirl6lxQIDAQAB");
        var config = new GoogleCloudLoggingSinkOptions { ProjectId = "fyves-384212" };
        Log.Logger = new LoggerConfiguration().WriteTo.GoogleCloudLogging(config).CreateLogger();
        services.AddControllers();
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Program).Assembly));
        services.AddScoped<IContentRepository, ContentRepository>();
        services.AddSingleton<IMessageBusClient, MessageBusClient>();
        services.AddHostedService<MessageBusSubscriber>();
        services.AddSingleton<IEventProcessor, EventProcessor>();
        //Console.WriteLine("using inMem Db");
        //services.AddDbContext<DbContextClass>(opt =>
        //opt.UseInMemoryDatabase("InMemory"));
        services.AddDbContext<DbContextClass>(opt =>
        {
            opt.UseNpgsql(Configuration.GetConnectionString("PostgreSqlConnection"));
        });

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "MyWebApi", Version = "v1" });
            //First we define the security scheme
            //c.AddSecurityDefinition("Bearer", //Name the security scheme
            //    new OpenApiSecurityScheme
            //    {
            //        Description = "JWT Authorization header using the Bearer scheme.",
            //        Type = SecuritySchemeType.Http, //We set the scheme type to http since we're using bearer authentication
            //        Scheme = JwtBearerDefaults.AuthenticationScheme //The name of the HTTP Authorization scheme to be used in the Authorization header. In this case "bearer".
            //    });
            //c.AddSecurityRequirement(new OpenApiSecurityRequirement{
            //        {
            //            new OpenApiSecurityScheme{
            //                Reference = new OpenApiReference{
            //                    Id = JwtBearerDefaults.AuthenticationScheme, //The name of the previously defined security scheme.
            //                    Type = ReferenceType.SecurityScheme
            //                }
            //            },new List<string>()
            //        }
            //    });
        });
        
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Backend v1"));
        }

        app.UseHttpsRedirection();

        app.UseRouting();

        //app.UseAuthentication();

        //app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}