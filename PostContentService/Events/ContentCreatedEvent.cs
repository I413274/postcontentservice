﻿using MediatR;

namespace PostContentService.Events
{
    public class ContentCreatedEvent : INotification
    {
        public ContentCreatedEvent(int id, string userID, string name, string title, string description)
        {
            Id = id;
            UserID = userID;
            UserName = name;
            Title = title;
            Description = description;
        }

        public int Id { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string Event { get; set; }
    }
}
