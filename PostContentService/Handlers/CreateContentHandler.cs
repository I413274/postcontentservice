﻿using MediatR;
using PostContentService.models;
using PostContentService.Queries;
using PostContentService.repository;
using Serilog;

namespace PostContentService.Handlers
{
    public class CreateContentHandler : IRequestHandler<CreateContentCommand, ContentModel>
    {
        private readonly IContentRepository _contentRepository;

        public CreateContentHandler(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }
        public async Task<ContentModel> Handle(CreateContentCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var content = new ContentModel()
                {
                    Id = command.Id,
                    UserID = command.UserID,
                    Name = command.Name,
                    Title = command.Title,
                    Description = command.Description,
                };

                Log.Information("Save content to the database");

                return await _contentRepository.AddContentAsync(content);
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                Log.Error(ex, "An error occurred while saving content to the database");
                throw;
            }
        }
    }
}
